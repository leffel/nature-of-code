Mover[] movers = new Mover[10];

void setup() {
  size(640, 360);
  for (int i = 0; i < movers.length; i++) {
    movers[i] = new Mover(i);
  }
}

void draw() {
  background(255);
  for (int i = 0; i < movers.length; i++) {
    movers[i].update();
    movers[i].checkEdges();
    movers[i].display();
  }
}

class Mover {

    PVector location;
    PVector velocity;
    PVector acceleration;
    float topspeed;
    float timeRed;
    float timeBlue;
    float timeGreen;
    float colorTimer;
    color c;
    
    Mover(int _seed) {
      location = new PVector(random(width), random(height));
      velocity = new PVector(0, 0);
      topspeed = 4;
      timeRed = _seed * 10000;
      timeBlue = _seed * 20000;
      timeGreen = _seed * 30000;
      colorTimer = 0.015;
    }
    
    void update(){
      PVector mouse = new PVector(mouseX, mouseY);
      acceleration = PVector.sub(mouse, location);
      acceleration.normalize();
      acceleration.mult(0.25);
      velocity.add(acceleration);
      velocity.limit(topspeed);
      location.add(velocity);
      
      float cRed = map(noise(timeRed), 0, 1, 150, 255);
      timeRed += colorTimer;
      float cBlue = map(noise(timeBlue), 0, 1, 150, 255);
      timeBlue += colorTimer;
      float cGreen = map(noise(timeGreen), 0, 1, 150, 255);
      timeGreen += colorTimer;
      c = color(cRed, cBlue, cGreen);
    }
    
    void display(){
      stroke(0);
      fill(c);
      ellipse(location.x, location.y, 20, 20);
    }
    
    void checkEdges() {
      if (location.x > width) {
        location.x = 0;
      } else if (location.x < 0) {
        location.x = width;
      }
      
      if (location.y > height) {
        location.y = 0;
      } else if (location.y < 0) {
        location.y = height;
      }
    }

}
