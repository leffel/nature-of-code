class Disc {

  PVector location;
  float radius;
  color c;
  boolean haste;
  
  Disc(float _x, float _y, float _radius, color _color, boolean _haste) {
    location = new PVector(_x, _y);
    c = _color;
    radius = _radius;
    haste = _haste;
  }
  
  void checkCollision(Mover mover){
    PVector distanceVect = PVector.sub(location, mover.location);
    boolean crossing = distanceVect.mag() <= radius + mover.radius;
    if (crossing && haste) {
      PVector friction = velocity.get();
      friction.normalize();
      friction.mult(1.2);
      mover.applyForce(friction);
    } else if (crossing) {
      PVector friction = velocity.get();
      friction.normalize();
      friction.mult(-1);
      mover.applyForce(friction);
    }
  }
  
  void display() {
    fill(0, 150);
    ellipse(location.x, location.y, radius*2, radius*2);
  }
}
