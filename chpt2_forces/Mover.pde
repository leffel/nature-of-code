class Mover {

  PVector location;
  PVector velocity;
  PVector acceleration;
  float topspeed;
  float timeRed;
  float timeBlue;
  float timeGreen;
  float colorTimer;
  color c;
  float mass, radius;

  Mover(int _seed) {
    location = new PVector(random(width), random(height));
    velocity = new PVector(random(-5, 5), random(-5, 5));
    acceleration = new PVector(0, 0);
    topspeed = 4;
    timeRed = _seed * 10000;
    timeBlue = _seed * 20000;
    timeGreen = _seed * 30000;
    colorTimer = 0.015;
    mass = (float) random(0.5, 4);
    radius = mass * 8;
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(topspeed);
    location.add(velocity);
    acceleration.mult(0);

    float cRed = map(noise(timeRed), 0, 1, 150, 255);
    timeRed += colorTimer;
    float cBlue = map(noise(timeBlue), 0, 1, 150, 255);
    timeBlue += colorTimer;
    float cGreen = map(noise(timeGreen), 0, 1, 150, 255);
    timeGreen += colorTimer;
    c = color(cRed, cBlue, cGreen);
  }

  void applyForce(PVector force) {
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }

  void display() {
    stroke(0);
    fill(c);
    ellipse(location.x, location.y, radius*2, radius*2);
  }

  void checkEdges() {
    if (location.x > width) {
      location.x = width;
      velocity.x *= -1;
    } else if (location.x < 0) {
      location.x = 0;
      velocity.x *= -1;
    }

    if (location.y > height) {
      velocity.y *= -1;
      location.y = height;
    } else if (location.y < 0) {
      velocity.y *= -1;
      location.y = 0;
    }
  }
}
