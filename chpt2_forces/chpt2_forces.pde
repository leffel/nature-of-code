Mover[] movers = new Mover[2];
Disc a_side;
Disc b_side;
float tw;

void setup() {
  size(360, 360);
  a_side = new Disc(width * 0.25, height * 0.5, 25, color(255, 255, 255), true);
  b_side = new Disc(width * 0.75, height * 0.5, 25, color(255, 255, 255), false);
  tw = random(0, 10000);
  for (int i = 0; i < movers.length; i++) {
    movers[i] = new Mover(i);
  }
}

void draw() {
  background(255);

  for (int i = 0; i < movers.length; i++) {
    a_side.checkCollision(movers[i]);
    b_side.checkCollision(movers[i]);
    movers[i].update();
    movers[i].checkEdges();
    movers[i].display();
  }
  
  a_side.display();
  b_side.display();
}
