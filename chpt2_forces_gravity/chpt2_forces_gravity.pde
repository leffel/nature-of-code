Planet[] planets = new Planet[9];
Sun sun;
float tw;

void setup() {
  size(360, 360);
  tw = random(0, 10000);
  sun = new Sun(width/2, height/2);
  for (int i = 0; i < planets.length; i++) {
    planets[i] = new Planet((i + 1) * 2);
  }
}

void draw() {
  background(255);
  sun.display();
  
  for (int i = 0; i < planets.length; i++) {
    Planet planet = planets[i];
    PVector s_f = sun.attract(planet);
    planet.applyForce(s_f);
    for (int j = 0; j < planets.length; j++) {
      if (i != j) {
        PVector p_f = planet.attract(planets[j]);
        planet.applyForce(p_f);
      }
    }
    planet.update();
    planet.display();
  }
}

class Sun {
  PVector location;
  color c;
  float mass, radius;
  
  Sun (float _x, float _y){
    location = new PVector(_x, _y);
    c = color(255, 255, 0);
    mass = 50;
    radius = mass * 2;
  }
  
  void display() {
    stroke(0);
    fill(c);
    ellipse(location.x, location.y, radius, radius);
  }
  
  PVector attract(Planet p){
    PVector force = PVector.sub(location, p.location);
    float distance = force.mag();
    distance = constrain(distance, 5, 25);
    float pull = (mass * p.mass) / (distance * distance);
    force.normalize();
    force.mult(pull);
    return force;
  }
}

class Planet {
  PVector location;
  PVector velocity;
  PVector acceleration;
  float topspeed;
  color c;
  float mass, radius;

  Planet(float _mass) {
    location = new PVector(random(width), random(height));
    velocity = new PVector(random(-1, 1), random(-1, 1));
    acceleration = new PVector(0, 0);
    mass = _mass;
    radius = mass * 2;
  }

  void update() {
    velocity.add(acceleration);
    velocity.limit(12);
    location.add(velocity);
    acceleration.mult(0);
  }

  void applyForce(PVector force) {
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }

  void display() {
    stroke(0);
    fill(c);
    ellipse(location.x, location.y, radius, radius);
  }
  
  PVector attract(Planet p){
    PVector force = PVector.sub(location, p.location);
    float distance = force.mag();
    distance = constrain(distance, 5, 100);
    float pull = (mass * p.mass) / (distance * distance);
    force.normalize();
    force.mult(pull);
    return force;
  }
}
