float angle = 0;
float aVelocity = 0.01;
float aAcceleration = 0.001;

void setup(){
  size(200, 200);
  background(255);
}

void draw(){
  int len = 40;
  background(255);
  fill(0);
  translate(width/2, height/2);
  rotate(angle);
  ellipse(-len, 0, 20, 20);
  line(-len, 0, len, 0);
  ellipse(len, 0, 20, 20);
  
  if (aVelocity < 5) {
    aVelocity += aAcceleration;
  }
  angle += aVelocity;
}
