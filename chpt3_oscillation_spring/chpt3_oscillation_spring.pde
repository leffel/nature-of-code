void setup() {
  size(640,360);
}
 
void draw() {
  background(255);
 
  float period = 120;
  float amplitude = 100;
  float x = amplitude * cos(TWO_PI * frameCount / period);
  float y = map(x, -amplitude, amplitude, 0, height * 0.8);
  stroke(0);
  fill(175);
  translate(width/2, 10);
  line(0,-10,0,y);
  ellipse(0,y,20,20);
}
