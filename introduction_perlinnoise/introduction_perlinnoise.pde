class Walker {
  PVector loc;
  float c;
  float size;
  float tx, ty, ts, tc;
  float incrementer;
  
  Walker(){
    tx = 0;
    ty = 10000;
    ts = 20000;
    tc = 30000;
    incrementer = 0.005;
  }
  
  void step() {
    float x = map(noise(tx), 0, 1, 0, width);
    float y = map(noise(ty), 0, 1, 0, height);
    loc = new PVector(x, y);
    
    c = map(noise(tc), 0, 1, 200, 255);
    size = map(noise(ts), 0, 1, 5, 60);
    
    tx += incrementer;
    ty += incrementer;
    ts += 0.01;
    tc += 0.01;
  }
  
  void display() {
    fill(100, 255, c);
    ellipse(loc.x, loc.y, size, size);
  }
}

Walker w;

void setup() {
  w = new Walker();
  size(640, 360);
}

void draw() {
  background(0);
  w.step();
  w.display();
}
